from distutils.core import setup, Extension
from Cython.Build import cythonize

extensions = [
    Extension("*", ["flask/*.pyx"], include_dirs=['.']),
    Extension("*", ["flask/*/*.pyx"], include_dirs=['.'])
]
setup(
        name='clask app',
        # ext_modules=[
        #     Extension("*", sources=["clask/*.py", "clask/json/*.py"])
        # ],
        # ext_modules=cythonize(['flask/*.pyx', 'flask/json/*.pyx']),
        # ext_modules=cythonize(['example/*.pyx', 'example/json/*.pyx',])
        ext_modules=cythonize(extensions),
)
